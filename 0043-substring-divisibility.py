#!/usr/bin/env python3
from itertools import permutations

# The number 1406357289 is a 0 to 9 pandigital number because it is made
# up of each of the digits 0 to 9 in some order, but it also has a rather
# interesting substring divisibility property.
#
# Let d[1] be the 1st digit, d[2] be the 2nd digit, and so on.  In this
# way, we note the following:
#
#   d[2] * d[3] * d[4] = 406 is divisible by 2
#   d[3] * d[4] * d[5] = 063 is divisible by 3
#   d[4] * d[5] * d[6] = 635 is divisible by 5
#   d[5] * d[6] * d[7] = 357 is divisible by 7
#   d[6] * d[7] * d[8] = 572 is divisible by 11
#   d[7] * d[8] * d[9] = 728 is divisible by 13
#   d[8] * d[9] * d[10] = 289 is divisible by 17
#
# Find the sum of all 0 to 9 pandigital numbers with this property.

def make_pandigitals():
    for digits in permutations('0123456789'):
        yield int(''.join(digits))

def slice_number(number, shift):
    return (number // pow(10, shift)) % 1000

def is_divisible(number):
    slices = (slice_number(number, n) for n in reversed(range(0, 7)))
    pairs = zip(slices, (2, 3, 5, 7, 11, 13, 17))
    return all(x % y == 0 for x, y in pairs)

try:
    print(sum(n for n in make_pandigitals() if is_divisible(n)))
except (OSError, KeyboardInterrupt):
    pass
