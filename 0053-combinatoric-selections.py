#!/usr/bin/env python3
from functools import lru_cache

from lib.bisectx import find_gt

# There are exactly ten ways of selecting three from five, 12345:
#
#   123, 124, 125, 134, 135, 145, 234, 235, 245 and 345
#
# In combinatorics, we use the notation:
#
#   (n)         n!
#   ( ) = -------------
#   (r)   r! * (n - r)!
#
# Where r <= n, n! = n * (n - 1) * ... * 3 * 2 * 1, and 0! = 1.
#
# It is not until 23 that a value exceeds one million:
#
#   (23)
#   (  ) = 1144066
#   (10)
#
# How many, not necessarily distinct, values of:
#
#   (n)
#   ( ) for 1 <= n <= 100
#   (r)
#
# Are greater than one million?
#
# N.B. Combinatoric values make up Pascal's triangle:
#
#   (0 r) =                 1
#   (1 r) =               1   -
#   (2 r) =             1   2   -
#   (3 r) =           1   3   -   -
#   (4 r) =         1   4   6   -   -
#   (5 r) =       1   5  10   -   -   -
#   (6 r) =     1   6  15  20   -   -   -
#   (7 r) =   1   7  21  35   -   -   -   -
#   (8 r) = 1   8  28  56  70   -   -   -   -

class CombinatoricArray:

    def __init__(self, base):
        self.base = base

    def __len__(self):
        return (self.base + 2) // 2

    def __getitem__(self, index):
        upper = get_factorial(self.base)
        lower1 = get_factorial(index)
        lower2 = get_factorial(self.base - index)
        return upper // (lower1 * lower2)

@lru_cache(maxsize=None)
def get_factorial(number):
    return number * get_factorial(number - 1) if number > 0 else 1

def get_combinations(array):
    try:
        start = find_gt(array, 1_000_000)
        return (array.base + 1) - (start * 2)
    except ValueError:
        return 0

try:
    print(sum(get_combinations(CombinatoricArray(n)) for n in range(1, 101)))
except (OSError, KeyboardInterrupt):
    pass
