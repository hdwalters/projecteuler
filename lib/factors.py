from functools import lru_cache

@lru_cache(maxsize=None)
def get_factors(number, proper=False):
    factors = set()
    factors.add(1)
    if not proper:
        factors.add(number)
    for divisor in range(2, (number // 2) + 1):
        if number % divisor == 0:
            for factor in get_factors(divisor):
                factors.add(factor)
    return sorted(factors)

@lru_cache(maxsize=None)
def is_perfect(number):
    factors = get_factors(number, True)
    return sum(factors) == number

@lru_cache(maxsize=None)
def is_abundant(number):
    factors = get_factors(number, True)
    return sum(factors) > number

@lru_cache(maxsize=None)
def is_deficient(number):
    factors = get_factors(number, True)
    return sum(factors) < number
