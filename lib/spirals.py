#   37<---------------------------31
#    |                             ^
#    |   17<-----------------13    |
#    |    |                   ^    |
#    |    |   05<-------03    |    |
#    |    |    |         |    |    |
#    |    |    |   01--->/    |    |
#    |    |    v              |    |
#    |    |   07------->09--->/    |
#    |    v                        |
#    |   21----------------->25--->/
#    v
#   43--------------------------->49

def make_diagonals():
    row = col = 0
    index = 1
    delta = 1
    yield row, col, index
    while True:
        yield row - delta, col + delta, index + (delta * 2)
        yield row - delta, col - delta, index + (delta * 4)
        yield row + delta, col - delta, index + (delta * 6)
        yield row + delta, col + delta, index + (delta * 8)
        index += delta * 8
        delta += 1
