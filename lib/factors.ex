Code.require_file "memoize.ex", "lib"

defmodule Factors do

  def get_factors(pid, number, options \\ []) do
    factors = find_factors(pid, number)
    if options[:proper], do: MapSet.delete(factors, number), else: factors
  end

  defp find_factors(pid, number) do
    Memoize.memoize pid, number, fn ->
      Stream.iterate(2, fn x -> x + 1 end)
      |> Stream.take_while(fn x -> x <= div(number, 2) end)
      |> Stream.filter(fn x -> rem(number, x) == 0 end)
      |> Stream.map(fn x -> find_factors(pid, x) end)
      |> Enum.reduce(MapSet.new, &MapSet.union/2)
      |> MapSet.put(1)
      |> MapSet.put(number)
    end
  end

end
