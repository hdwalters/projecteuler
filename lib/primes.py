import itertools
import math
from functools import lru_cache

def make_primes(start=2, stop=None):
    values = itertools.count(start) if stop is None else range(start, stop)
    for value in values:
        if is_prime(value):
            yield value

def make_composites(start=2, stop=None):
    values = itertools.count(start) if stop is None else range(start, stop)
    for value in values:
        if not is_prime(value):
            yield value

@lru_cache(maxsize=None)
def is_prime(number):
    if number < 2:
        return False
    else:
        limit = math.floor(math.sqrt(number)) + 1
        for divisor in itertools.takewhile(lambda x: x < limit, _make_divisors()):
            if is_prime(divisor) and (number % divisor == 0):
                return False
        return True

def _make_divisors():
    yield 2
    yield 3
    base = 6
    while True:
        yield base - 1
        yield base + 1
        base += 6
