import bisect

def find_eq(values, value):
    index = bisect.bisect_left(values, value)
    if index < len(values) and values[index] == value:
        return index
    raise ValueError

def find_lt(values, value):
    index = bisect.bisect_left(values, value)
    if index > 0:
        return index - 1
    raise ValueError

def find_le(values, value):
    index = bisect.bisect_right(values, value)
    if index > 0:
        return index - 1
    raise ValueError

def find_gt(values, value):
    index = bisect.bisect_right(values, value)
    if index < len(values):
        return index
    raise ValueError

def find_ge(values, value):
    index = bisect.bisect_left(values, value)
    if index < len(values):
        return index
    raise ValueError
