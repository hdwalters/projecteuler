Code.require_file "memoize.ex", "lib"

defmodule Primes do

  def make_primes(pid) do
    Stream.iterate(1, fn x -> x + 1 end)
    |> Stream.filter(fn x -> is_prime?(pid, x) end)
  end

  def is_prime?(_pid, number) when number < 2 do
    false
  end

  def is_prime?(pid, number) do
    Memoize.memoize pid, number, fn ->
      limit = round(:math.sqrt(number))
      case make_divisors()
      |> Stream.take_while(fn x -> x <= limit end)
      |> Stream.filter(fn x -> is_prime?(pid, x) end)
      |> Stream.filter(fn x -> rem(number, x) == 0 end)
      |> Enum.fetch(0) do
        {:ok, _} -> false
        :error -> true
      end
    end
  end

  defp make_divisors do
    stream = Stream.iterate(6, fn x -> x + 6 end)
    stream = Stream.flat_map(stream, fn x -> [x - 1, x + 1] end)
    Stream.concat(2..3, stream)
  end

end
