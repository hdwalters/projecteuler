import os
import sys

def read_lines():
    dirname, filename = os.path.split(sys.argv[0])
    basename, _ = os.path.splitext(filename)
    path = os.path.join(dirname, 'data', f'{basename}.txt')
    with open(path) as file:
        for line in file:
            yield line.rstrip()
