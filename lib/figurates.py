from itertools import count
from math import sqrt

# To solve quadratic equation ax^2 + bx + c = 0, use the formula:
#
#        -b +/- sqrt(b^2 - 4ac)
#   x = ------------------------
#                  2a
#
#   F[3,n] = n(n + 1) / 2
#            n^2 + n - 2F = 0
#            a=1 b=1 c=-2
#
#   F[5,n] = n(3n - 1) / 2
#            3n^2 - n - 2F = 0
#            a=3 b=-1 c=-2
#
#   F[6,n] = n(2n - 1)
#            2n^2 - n - F = 0
#            a=2 b=-1 c=-1

def solve_quadratic(a, b, c):
    return (-b + sqrt((b * b) - (4 * a * c))) / (2 * a)

def is_triangular(number):
    index = round(solve_quadratic(1, 1, number * -2))
    return get_triangular(index) == number

def is_pentagonal(number):
    index = round(solve_quadratic(3, -1, number * -2))
    return get_pentagonal(index) == number

def is_hexagonal(number):
    index = round(solve_quadratic(2, -1, number * -1))
    return get_hexagonal(index) == number

def get_triangular(index):
    return index * (index + 1) // 2

def get_pentagonal(index):
    return index * ((index * 3) - 1) // 2

def get_hexagonal(index):
    return index * ((index * 2) - 1)

def make_triangulars():
    for index in count(1):
        yield get_triangular(index)

def make_pentagonals():
    for index in count(1):
        yield get_pentagonal(index)

def make_hexagonals():
    for index in count(1):
        yield get_hexagonal(index)
