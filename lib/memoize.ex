defmodule Memoize do

  use Agent

  def start_link! do
    {:ok, pid} = Agent.start_link(fn -> %{} end)
    pid
  end

  def memoize(pid, key, func) do
    key = {func, key}
    case Agent.get(pid, &Map.get(&1, key)) do
      nil ->
        result = func.()
        Agent.update(pid, &Map.put(&1, key, result))
        result
      cached ->
        cached
    end
  end

end
