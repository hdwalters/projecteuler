#!/usr/bin/env python3
from lib.factors import get_factors

# We shall say that an n-digit number is pandigital if it makes use of all
# the digits 1 to n exactly once; for example, the 5-digit number 15234 is
# 1 through 5 pandigital.
#
# The product 7254 is unusual, as the identity 39 * 186 = 7254, containing
# multiplicand, multiplier and product is 1 through 9 pandigital.
#
# Find the sum of all products whose multiplicand, multiplier and product
# identity can be written as a 1 through 9 pandigital.  Hint: some products
# can be obtained in more than one way so be sure to only include it once
# in your sum.
#
# N.B. 10000 and larger cannot be a product in a pandigital identity,
# because if it were, its divisors would be of the order of 100 * 100,
# 99 * 101, 10 * 1000, 9 * 1001 etc; and all of these identities would
# contain ten or eleven digits.

def is_pandigital(first, second, number):
    digits = f'{first}{second}{number}'
    digits = sorted(digits)
    return digits == ['1', '2', '3', '4', '5', '6', '7', '8', '9']

def test_number(number):
    factors = get_factors(number, True)
    for first in factors:
        second = number // first
        if first > second:
            break
        if is_pandigital(first, second, number):
            return True
    return False

try:
    print(sum(x for x in range(10000) if test_number(x)))
except (OSError, KeyboardInterrupt):
    pass
