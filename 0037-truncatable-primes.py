#!/usr/bin/env python3
from itertools import *

from lib.primes import *

# The number 3797 has an interesting property.  Being prime itself, it is
# possible to continuously remove digits from left to right, and remain
# prime at each stage: 3797, 797, 97, and 7.  Similarly we can work from
# right to left: 3797, 379, 37, and 3.
#
# Find the sum of the only eleven primes that are both truncatable from
# left to right and right to left.
#
# 2, 3, 5, and 7 are not considered to be truncatable primes.

def make_truncated(number):
    text = str(number)
    length = len(text)
    for index in reversed(range(1, length)):
        yield int(text[index:])
        yield int(text[:(length - index)])
    yield number

def is_truncatable(number):
    return all(is_prime(n) for n in make_truncated(number))

def make_truncatable():
    for number in make_primes(10):
        if is_truncatable(number):
            yield number

try:
    print(sum(n for n in islice(make_truncatable(), 0, 11)))
except (OSError, KeyboardInterrupt):
    pass
