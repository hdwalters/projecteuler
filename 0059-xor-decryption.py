#!/usr/bin/env python3
import string
from itertools import *

from lib.data import *

# Each character on a computer is assigned a unique code, and the preferred
# standard is ASCII (American Standard Code for Information Interchange).
# For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.
#
# A modern encryption method is to take a text file, convert the bytes to
# ASCII, then XOR each byte with a given value, taken from a secret key.
# The advantage with the XOR function is that using the same encryption
# key on the ciphertext, restores the plaintext; e.g. 65 XOR 42 = 107,
# then 107 XOR 42 = 65.
#
# For unbreakable encryption, the key is the same length as the plaintext
# message, and the key is made up of random bytes.  The user would keep the
# encrypted message and the encryption key in different locations, and
# without both "halves", it is impossible to decrypt the message.
#
# Unfortunately, this method is impractical for most users, so the modified
# method is to use a password as a key.  If the password is shorter than
# the message, which is likely, the key is repeated cyclically throughout
# the message.  The balance for this method is using a sufficiently long
# password key for security, but short enough to be memorable.
#
# Your task has been made easy, as the encryption key consists of three
# lower case characters.  Using the file containing the encrypted ASCII
# codes, and the knowledge that the plaintext must contain common English
# words, decrypt the message and find the sum of the ASCII values in the
# original plaintext.

def read_chars():
    for line in read_lines():
        for char in line.split(','):
            yield int(char)

def get_slice(ciphertext, index):
    return list(islice(ciphertext, index, None, 3))

def get_score(plaintext):
    return sum(1 for x in plaintext if chr(x).isalpha())

def get_best(ciphertext):
    candidates = [list(x ^ ord(k) for x in ciphertext) for k in string.ascii_lowercase]
    return max(candidates, key=get_score)

try:
    ciphertext = list(read_chars())
    plaintext = [get_best(get_slice(ciphertext, i)) for i in range(3)]
    plaintext = list(chain.from_iterable(zip(*plaintext)))
    print(sum(plaintext))
    # print(''.join(chr(c) for c in plaintext))
except (OSError, KeyboardInterrupt):
    pass
