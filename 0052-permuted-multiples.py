#!/usr/bin/env python3
from itertools import count

# It can be seen that the number 125874 and its double 251748 contain
# exactly the same digits, but in a different order.
#
# Find the smallest positive integer x, such that 2x, 3x, 4x, 5x and 6x
# contain the same digits.

def get_digits(number):
    return sorted(str(number))

def find_numbers():
    for number in count(1):
        digits = get_digits(number)
        if all(get_digits(number * m) == digits for m in range(2, 7)):
            yield number

try:
    print(next(find_numbers()))
except (OSError, KeyboardInterrupt):
    pass
