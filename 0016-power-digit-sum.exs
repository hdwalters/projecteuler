#!/usr/bin/env elixir

# 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
#
# What is the sum of the digits of the number 2^1000?

defmodule Pow do

  require Integer

  def pow(_, 0) do
    1
  end

  def pow(x, n) when Integer.is_odd(n) do
    x * pow(x, n - 1)
  end

  def pow(x, n) do
    result = pow(x, div(n, 2))
    result * result
  end

end

Pow.pow(2, 1000)
|> Integer.digits
|> Enum.sum
|> IO.puts
