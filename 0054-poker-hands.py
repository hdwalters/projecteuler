#!/usr/bin/env python3
import itertools
from enum import IntEnum
from functools import total_ordering

from lib.data import read_lines

# In the card game poker, a hand consists of five cards and are ranked,
# from lowest to highest, in the following way:
#
#   High Card - Highest value card.
#   One Pair - Two cards of the same value.
#   Two Pairs - Two different pairs.
#   Three of a Kind - Three cards of the same value.
#   Straight - All cards are consecutive values.
#   Flush - All cards of the same suit.
#   Full House - Three of a kind and a pair.
#   Four of a Kind - Four cards of the same value.
#   Straight Flush - All cards are consecutive values of same suit.
#   Royal Flush - Ten, Jack, Queen, King, Ace, in same suit.
#
# The cards are valued in the order:
#
#   2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
#
# If two players have the same ranked hands then the rank made up of the
# highest value wins; for example, a pair of eights beats a pair of fives
# (see example 1 below).  But if two ranks tie, for example, both players
# have a pair of queens, then highest cards in each hand are compared
# (see example 4 below); if the highest cards tie then the next highest
# cards are compared, and so on.
#
# Consider the following five hands dealt to two players:
#
#   Hand  Player 1             Player 2             Winner
#   ----- -------------------- -------------------- ---------
#   1     5H 5C 6S 7S KD       2C 3S 8S 8D TD       Player 2
#         Pair of Fives        Pair of Eights
#   ----- -------------------- -------------------- ---------
#   2     5D 8C 9S JS AC       2C 5C 7D 8S QH       Player 1
#         Highest card Ace     Highest card Queen
#   ----- -------------------- -------------------- ---------
#   3     2D 9C AS AH AC       3D 6D 7D TD QD       Player 2
#         Three Aces           Flush with Diamonds
#   ----- -------------------- -------------------- ---------
#   4     4D 6S 9H QH QC       3D 6D 7H QD QS       Player 1
#         Pair of Queens       Pair of Queens
#         Highest card Nine    Highest card Seven
#   ----- -------------------- -------------------- ---------
#   5     2H 2D 4C 4D 4S       3C 3D 3S 9S 9D       Player 1
#         Full House           Full House
#         With Three Fours     with Three Threes
#
# The file, poker.txt, contains one-thousand random hands dealt to two
# players.  Each line of the file contains ten cards, separated by a
# single space: the first five are Player 1's cards and the last five
# are Player 2's cards.  You can assume that all hands are valid (no
# invalid characters or repeated cards), each player's hand is in no
# specific order, and in each hand there is a clear winner.
#
# How many hands does Player 1 win?

VALUES = '23456789TJQKA'
SUITS = 'CDHS'

class Rank(IntEnum):
    UNKNOWN = 0
    HIGH_CARD = 1
    ONE_PAIR = 2
    TWO_PAIRS = 3
    THREE_KIND = 4
    STRAIGHT = 5
    FLUSH = 6
    FULL_HOUSE = 7
    FOUR_KIND = 8
    STRAIGHT_FLUSH = 9

@total_ordering
class Card:

    def __init__(self, card):
        self.value = VALUES.index(card[0])
        self.suit = SUITS.index(card[1])

    def __str__(self):
        return f'{VALUES[self.value]}{SUITS[self.suit]}'

    def __repr__(self):
        return f"{self.__class__.__name__}('{self}')"

    def __eq__(self, other):
        return self.make_key() == other.make_key()

    def __lt__(self, other):
        return self.make_key() < other.make_key()

    def make_key(self):
        return self.value, self.suit

@total_ordering
class Hand:

    def __init__(self, hand):
        self.cards = sorted((Card(card) for card in hand), reverse=True)

    def __str__(self):
        return ' '.join(str(card) for card in self.cards)

    def __eq__(self, other):
        return self.make_key() == other.make_key()

    def __lt__(self, other):
        return self.make_key() < other.make_key()

    def make_key(self):
        if key := self._get_straight_flush():
            return Rank.STRAIGHT_FLUSH, key
        elif key := self._get_four_kind():
            return Rank.FOUR_KIND, key
        elif key := self._get_full_house():
            return Rank.FULL_HOUSE, key
        elif key := self._get_flush():
            return Rank.FLUSH, key
        elif key := self._get_straight():
            return Rank.STRAIGHT, key
        elif key := self._get_three_kind():
            return Rank.THREE_KIND, key
        elif key := self._get_two_pairs():
            return Rank.TWO_PAIRS, key
        elif key := self._get_one_pair():
            return Rank.ONE_PAIR, key
        elif key := self._get_high_card():
            return Rank.HIGH_CARD, key
        else:
            return Rank.UNKNOWN,

    def _get_straight_flush(self):
        if self._get_straight() and self._get_flush():
            return self.cards
        return None

    def _get_four_kind(self):
        for cards in itertools.combinations(self.cards, 4):
            if cards[0].value == cards[1].value == cards[2].value == cards[3].value:
                return cards
        return None

    def _get_full_house(self):
        if (self.cards[0].value == self.cards[1].value == self.cards[2].value) and (self.cards[3].value == self.cards[4].value):
            return self.cards
        if (self.cards[0].value == self.cards[1].value) and (self.cards[2].value == self.cards[3].value == self.cards[4].value):
            return self.cards
        return None

    def _get_flush(self):
        if all(c.suit == self.cards[0].suit for c in itertools.islice(self.cards, 1, 5)):
            return self.cards
        return None

    def _get_straight(self):
        diffs = [x.value - y.value for x, y in zip(self.cards, itertools.islice(self.cards, 1, 5))]
        if all(d == 1 for d in diffs):
            return self.cards
        return None

    def _get_three_kind(self):
        for cards in itertools.combinations(self.cards, 3):
            if cards[0].value == cards[1].value == cards[2].value:
                return cards
        return None

    def _get_two_pairs(self):
        for cards in itertools.combinations(self.cards, 4):
            if (cards[0].value == cards[1].value) and (cards[2].value == cards[3].value):
                return cards
        return None

    def _get_one_pair(self):
        for cards in itertools.combinations(self.cards, 2):
            if cards[0].value == cards[1].value:
                return cards
        return None

    def _get_high_card(self):
        card = max(self.cards)
        return card,

def make_hands():
    for line in read_lines():
        cards = line.split()
        hand1 = Hand(cards[0:5])
        hand2 = Hand(cards[5:10])
        yield hand1, hand2

try:
    print(sum(1 for hand1, hand2 in make_hands() if hand1 > hand2))
except (OSError, KeyboardInterrupt):
    pass
