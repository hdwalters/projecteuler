#!/usr/bin/env python3
from fractions import Fraction
from functools import lru_cache

# It is possible to show that the square root of two can be expressed
# as an infinite continued fraction.
#
#             1
#   1 + --------------- = sqrt(2)
#               1
#       2 + -----------
#                 1
#           2 + -------
#                    1
#               2 + ---
#                   ...
#
# By expanding this for the first four iterations, we get:
#
#                    1     3
#               1 + --- = --- = 1.5
#                    2     2
#
#                  1       7
#           1 + ------- = --- = 1.4
#                    1     5
#               2 + ---
#                    2
#
#                1         17
#       1 + ----------- = ---- = 1.41666...
#                  1       12
#           2 + -------
#                    1
#               2 + ---
#                    2
#
#              1           41
#   1 + --------------- = ---- = 1.41379...
#                1         29
#       2 + -----------
#                  1
#           2 + -------
#                    1
#               2 + ---
#                    2
#
# The next three expansions are 99/70, 239/169 and 577/408, but the eighth
# expansion 1393/985 is the first example where the number of digits in
# the numerator exceeds the number of digits in the denominator.
#
# In the first thousand expansions, how many fractions contain a numerator
# with more digits than the denominator?

@lru_cache(maxsize=None)
def get_fraction(count, increment=1):
    if count > 0:
        return increment + (1 / get_fraction(count - 1, 2))
    else:
        return increment + Fraction(1, 2)

def make_fractions(stop):
    for count in range(stop):
        fraction = get_fraction(count)
        if len(str(fraction.numerator)) > len(str(fraction.denominator)):
            yield fraction

try:
    print(sum(1 for f in make_fractions(1000)))
except (OSError, KeyboardInterrupt):
    pass
