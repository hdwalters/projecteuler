#!/usr/bin/env python3
from lib.primes import *

# The prime 41, can be written as the sum of six consecutive primes:
#
#   41 = 2 + 3 + 5 + 7 + 11 + 13
#
# This is the longest sum of consecutive primes that adds to a prime
# below one-hundred.
#
# The longest sum of consecutive primes below one-thousand that adds to
# a prime, contains 21 terms, and is equal to 953.
#
# Which prime, below one million, can be written as the sum of the most
# consecutive primes?
#
# N.B. The first 100 candidates can be written as follows:
#
#   77 = 2 + 3 + 5 + 7 + 11 + 13 + 17 + 19
#   98 =     3 + 5 + 7 + 11 + 13 + 17 + 19 + 23
#  --------------------------------------------------
#   58 = 2 + 3 + 5 + 7 + 11 + 13 + 17
#   75 =     3 + 5 + 7 + 11 + 13 + 17 + 19
#   95 =         5 + 7 + 11 + 13 + 17 + 19 + 23
#  --------------------------------------------------
#   41 = 2 + 3 + 5 + 7 + 11 + 13
#   56 =     3 + 5 + 7 + 11 + 13 + 17
#   72 =         5 + 7 + 11 + 13 + 17 + 19
#   90 =             7 + 11 + 13 + 17 + 19 + 23
#  --------------------------------------------------
#   28 = 2 + 3 + 5 + 7 + 11
#   39 =     3 + 5 + 7 + 11 + 13
#   53 =         5 + 7 + 11 + 13 + 17
#   67 =             7 + 11 + 13 + 17 + 19
#   83 =                 11 + 13 + 17 + 19 + 23
#  --------------------------------------------------
#   17 = 2 + 3 + 5 + 7
#   26 =     3 + 5 + 7 + 11
#   36 =         5 + 7 + 11 + 13
#   48 =             7 + 11 + 13 + 17
#   60 =                 11 + 13 + 17 + 19
#   72 =                      13 + 17 + 19 + 23
#   88 =                           17 + 19 + 23 + 29
#  --------------------------------------------------
#   10 = 2 + 3 + 5
#   15 =     3 + 5 + 7
#   23 =         5 + 7 + 11
#   31 =             7 + 11 + 13
#   41 =                 11 + 13 + 17
#   49 =                      13 + 17 + 19
#   59 =                           17 + 19 + 23
#   71 =                                19 + 23 + 29
#   83 =                                     23 + 29 + 31
#   97 =                                          29 + 31 + 37

def advance_prime(number):
    while True:
        number += 1
        if is_prime(number):
            return number

def reverse_prime(number):
    while True:
        number -= 1
        if is_prime(number):
            return number

def find_initial(limit):
    start = 2
    total = 0
    for stop in make_primes(start):
        if total + stop < limit:
            total += stop
        else:
            return start, stop, total

def make_candidates(start, stop, total, limit):
    while total < limit:
        yield total
        total = total + stop - start
        start = advance_prime(start)
        stop = advance_prime(stop)

def make_totals(limit):
    start, stop, total = find_initial(limit)
    while True:
        for candidate in make_candidates(start, stop, total, limit):
            if is_prime(candidate):
                yield candidate
        stop = reverse_prime(stop)
        total -= stop

try:
    print(next(make_totals(1_000_000)))
except (OSError, KeyboardInterrupt):
    pass
