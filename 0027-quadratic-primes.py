#!/usr/bin/env python3
from itertools import *

from lib.primes import *

# Euler discovered the remarkable quadratic formula:
#
# n^2 + n + 41
#
# It turns out that the formula will produce 40 primes for the consecutive
# integer values 0 <= n <= 39.  However:
#
# When n = 40, 40^2 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41.
# When n = 41, 41^2 + 41 + 41 is clearly divisible by 41.
#
# The incredible formula n^2 - 79n + 1601 was discovered, which produces
# 80 primes for the consecutive values 0 <= n <= 79. The product of the
# coefficients, -79 and 1601, is -126479.
#
# Considering quadratics of the form:
#
# n^2 + an + b (where |a| < 1000 and |b| <= 1000)
#
# Find the product of the coefficients a and b, for the quadratic expression
# that produces the maximum number of primes for consecutive values of n,
# starting with n = 0.

def count_primes(pair):
    a, b = pair
    formula = lambda n: is_prime((n * n) + (a * n) + b)
    return sum(1 for _ in takewhile(formula, count()))

def make_pairs():
    for a in range(-999, 1000):
        for b in range(-1000, 1001):
            yield a, b

try:
    besta, bestb = max(make_pairs(), key=count_primes)
    print(besta * bestb)
except (OSError, KeyboardInterrupt):
    pass
