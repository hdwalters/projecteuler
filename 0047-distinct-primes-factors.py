#!/usr/bin/env python3

# The first two consecutive numbers with two distinct prime factors are:
#
#   14 = 2 * 7
#   15 = 3 * 5
#
# The first three consecutive numbers with three distinct prime factors are:
#
#   644 = 2^2 * 7 * 23
#   645 = 3 * 5 * 43
#   646 = 2 * 17 * 19
#
# Find the first four consecutive integers with four distinct prime factors
# each.  What is the first of these numbers?

try:
    print('(unknown)')
except (OSError, KeyboardInterrupt):
    pass
