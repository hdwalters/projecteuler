#!/usr/bin/env python3
from itertools import *

from lib.primes import *

# The primes 3, 7, 109, and 673, are quite remarkable.  By taking any
# two primes and concatenating them in any order the result will always
# be prime.  For example, taking 7 and 109, both 7109 and 1097 are prime.
# The sum of these four primes, 792, represents the lowest sum for a set
# of four primes with this property.
#
# Find the lowest sum for a set of five primes for which any two primes
# concatenate to produce another prime.

@lru_cache(maxsize=None)
def test_pair(x, y):
    return is_prime(int(f'{x}{y}')) and is_prime(int(f'{y}{x}'))

def test_pairs(xs, y):
    return all(test_pair(x, y) for x in xs)

def make_candidates(start=39):
    for total in count(start):
        for a in make_primes(3, total):
            for b in make_primes(a + 1, (total - a) // 4):
                if test_pair(a, b):
                    for c in make_primes(b + 1, (total - a - b) // 3):
                        if test_pairs((a, b), c):
                            for d in make_primes(c + 1, (total - a - b - c) // 2):
                                if test_pairs((a, b, c), d):
                                    e = total - a - b - c - d
                                    if is_prime(e) and test_pairs((a, b, c, d), e):
                                        yield total

try:
    print('(unknown)')
except (OSError, KeyboardInterrupt):
    pass
