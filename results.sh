#!/usr/bin/env bash
cd ${1:-$(dirname $0)}

# Create padding string.
length=48
length=$((length-1))
padding=$(printf '%*s' $length | tr ' ' '.')

# Filter previous scripts.
if test -f results.out; then
    reuse=$(wc -l results.out | cut -d' ' -f1)
    echo "Reusing first $reuse results" >/dev/stderr
    function filter { tail +$((reuse+1)) "$@"; }
else
    function filter { cat "$@"; }
fi

# Execute new scripts.
for script in $(find . -maxdepth 1 -type f -executable -regex './[0-9]+-.+' | sort -V | filter); do
    padded="${script#\./} $padding"
    result=$($script)
    echo "${padded:0:$length} $result" | tee -a results.out
done
