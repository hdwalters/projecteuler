#!/usr/bin/env elixir

# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which
#
# a^2 + b^2 = c^2
#
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
#
# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product a * b * c.

defmodule Test do

  def find_triplet(total) do
    1..total
    |> Stream.map(fn a -> find_triplet(total, a) end)
    |> Stream.filter(fn x -> !is_nil(x) end)
    |> Enum.fetch!(0)
  end

  defp find_triplet(total, a) do
    case (a + 1)..total
    |> Stream.map(fn b -> {a, b, make_third(a, b)} end)
    |> Stream.take_while(fn {a, b, c} -> (a + b + c) <= total end)
    |> Stream.filter(fn {a, b, c} -> (a + b + c) == total && (a * a) + (b * b) == (c * c) end)
    |> Enum.fetch(0) do
      {:ok, triplet} -> triplet
      :error -> nil
    end
  end

  defp make_third(a, b) do
    round(:math.sqrt((a * a) + (b * b)))
  end

end

{a, b, c} = Test.find_triplet(1000)
IO.puts a * b * c
