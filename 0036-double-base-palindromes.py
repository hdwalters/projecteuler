#!/usr/bin/env python3
import re

# The decimal number 585 = 1001001001 (binary) is palindromic in both bases.
#
# Find the sum of all numbers, less than one million, which are palindromic
# in base 10 and base 2 (please note that the palindromic number, in either
# base, may not include leading zeros).

def is_palindrome(number):
    if re.search(r'0$', number):
        return False
    else:
        reverse = ''.join(reversed(number))
        return reverse == number

def test_palindrome(number):
    return is_palindrome(f'{number}') and is_palindrome(f'{number:b}')

try:
    print(sum(n for n in range(1_000_000) if test_palindrome(n)))
except (OSError, KeyboardInterrupt):
    pass
