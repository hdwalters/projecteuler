#!/usr/bin/env python3
from lib.primes import is_prime
from lib.spirals import make_diagonals

# Starting with 1 and spiralling anticlockwise in the following way, a
# square spiral with side length 7 is formed.
#
#   [37]  36   35   34   33   32  [31]
#    38  [17]  16   15   14  [13]  30
#    39   18  [05]  04  [03]  12   29
#    40   19   06   01   02   11   28
#    41   20  [07]  08   09   10   27
#    42   21   22   23   24   25   26
#   [43]  44   45   46   47   48   49
#
# It is interesting to note that the odd squares lie along the bottom
# right diagonal, but what is more interesting is that 8 out of the 13
# numbers lying along both diagonals are prime; that is, a ratio of
# 8/13 = 62%.
#
# If one complete new layer is wrapped around the spiral above, a square
# spiral with side length 9 will be formed.  If this process is continued,
# what is the side length of the square spiral for which the ratio of
# primes along both diagonals first falls below 10%?

def make_lengths():
    total = count = 0
    for row, col, index in make_diagonals():
        total += 1
        if is_prime(index):
            count += 1
        if row > 0 and col > 0:
            percentage = count * 100 // total
            if percentage < 10:
                yield (row * 2) + 1

try:
    print(next(make_lengths()))
except (OSError, KeyboardInterrupt):
    pass
