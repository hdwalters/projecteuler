#!/usr/bin/env python3
from fractions import Fraction
from functools import reduce
from operator import mul

# The fraction 49/98 is a curious fraction, as an inexperienced
# mathematician in attempting to simplify it may incorrectly believe
# that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.
#
# We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
#
# There are exactly four non-trivial examples of this type of fraction,
# less than one in value, and containing two digits in the numerator and
# denominator.
#
# If the product of these four fractions is given in its lowest common
# terms, find the value of the denominator.

def is_curious(numer, denom):
    fraction = Fraction(numer, denom)
    numer10, numer1 = numer // 10, numer % 10
    denom10, denom1 = denom // 10, denom % 10
    if denom1 > 0:
        if numer10 == denom10 and Fraction(numer1, denom1) == fraction:
            return True
        if numer1 == denom10 and Fraction(numer10, denom1) == fraction:
            return True
        if numer10 == denom1 and denom10 > 0 and Fraction(numer1, denom10) == fraction:
            return True
        if numer1 == denom1 and denom10 > 0 and Fraction(numer10, denom10) == fraction:
            return True
    return False

try:
    pairs = ((n, d) for n in range(10, 100) for d in range(n + 1, 100))
    fractions = (Fraction(n, d) for n, d in pairs if is_curious(n, d))
    product = reduce(mul, fractions)
    print(product.denominator)
except (OSError, KeyboardInterrupt):
    pass
