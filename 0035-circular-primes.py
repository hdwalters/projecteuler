#!/usr/bin/env python3
import re

from lib.primes import is_prime

# The number, 197, is called a circular prime because all rotations of
# the digits: 197, 971, and 719, are themselves prime.
#
# There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31,
# 37, 71, 73, 79, and 97.
#
# How many circular primes are there below one million?
#
# N.B. No number containing "0", "4", "6" or "8", and no number of two
# or more digits containing "2", can be a circular prime, because one of
# its rotations will be even.

def make_rotations(number):
    length = len(number)
    for index in range(length):
        yield int(number[index:length] + number[0:index])

def is_circular(number):
    number = str(number)
    if re.search(r'^([13579]+|2)$', number):
        return all(is_prime(n) for n in make_rotations(number))
    else:
        return False

try:
    print(sum(1 for n in range(1_000_000) if is_circular(n)))
except (OSError, KeyboardInterrupt):
    pass
