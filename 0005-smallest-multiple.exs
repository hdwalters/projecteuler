#!/usr/bin/env elixir

# 2520 is the smallest number that can be divided by each of the numbers
# from 1 to 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible by all of
# the numbers from 1 to 20?

defmodule Test do

  def is_divisible?(number, maximum) do
    1..maximum
    |> Stream.map(fn x -> rem(number, x) == 0 end)
    |> Enum.all?
  end

  def find_smallest(working, minimum, maximum) do
    case minimum..maximum
    |> Stream.filter(fn x -> rem(working, x) == 0 end)
    |> Stream.filter(fn x -> is_divisible?(div(working, x), maximum) end)
    |> Enum.fetch(0) do
      {:ok, divisor} -> find_smallest(div(working, divisor), divisor, maximum)
      :error -> working
    end
  end

end

starting = 1..20 |> Enum.reduce(&*/2)
IO.puts Test.find_smallest(starting, 2, 20)
