#!/usr/bin/env python3
from lib.primes import *

# By replacing the 1st digit of the 2-digit number *3, it turns out that
# six of the nine possible values 13, 23, 43, 53, 73 and 83 are all prime.
#
# By replacing the 3rd and 4th digits of 56**3 with the same digit, this
# 5-digit number is the first example having seven primes among the ten
# generated numbers, yielding the family 56003, 56113, 56333, 56443, 56663,
# 56773 and 56993.  Consequently 56003, being the first member of this
# family, is the smallest prime with this property.
#
# Find the smallest prime which, by replacing part of the number (not
# necessarily adjacent digits) with the same digit, is part of an eight
# prime value family.

def test_zeroes(number, bitfield):
    digit = '1' if bitfield[0] else '0'
    return all(n == digit if b else True for n, b in zip(number, bitfield))

def replace_digits(number, bitfield, digit):
    return int(''.join(digit if b else n for n, b in zip(number, bitfield)))

def get_candidate(number, bitfield):
    digits = '123456789' if bitfield[0] else '0123456789'
    return [n for d in digits if is_prime(n := replace_digits(number, bitfield, d))]

def make_candidates(required):
    for number in itertools.count(1):
        number = str(number)
        length = len(number)
        for bitfield in itertools.product((False, True), repeat=length):
            if any(bitfield) and test_zeroes(number, bitfield):
                candidate = get_candidate(number, bitfield)
                if len(candidate) >= required:
                    yield int(number)

try:
    print(next(make_candidates(8)))
except (OSError, KeyboardInterrupt):
    pass
