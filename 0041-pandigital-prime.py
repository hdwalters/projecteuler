#!/usr/bin/env python3
from itertools import permutations

from lib.primes import is_prime

# We shall say that an n digit number is pandigital if it makes use of
# all the digits 1 to n exactly once.  For example, 2143 is a 4 digit
# pandigital and is also prime.
#
# What is the largest n digit pandigital prime that exists?

def make_pandigitals():
    for count in range(8):
        sequence = '987654321'[count:]
        for digits in permutations(sequence):
            yield int(''.join(digits))

def find_result():
    for number in make_pandigitals():
        if is_prime(number):
            return number

try:
    print(find_result())
except (OSError, KeyboardInterrupt):
    pass
