#!/usr/bin/env python3
from lib.primes import *

# The arithmetic sequence, 1487, 4817, 8147, in which each of the terms
# increases by 3330, is unusual in two ways: (i) each of the three terms
# are prime, and, (ii) each of the 4-digit numbers are permutations of
# one another.
#
# There are no arithmetic sequences made up of three 1-, 2-, or 3-digit
# primes exhibiting this property, but there is one other 4-digit increasing
# sequence.
#
# What 12-digit number do you form by concatenating the three terms in
# this sequence?

def get_digits(number):
    return str(sorted(str(number)))

def make_sequences():
    for first in make_primes(1000, 10000):
        digits = get_digits(first)
        for second in make_primes(first + 2, 10000):
            if get_digits(second) == digits:
                diff = second - first
                third = second + diff
                if third >= 10000:
                    break
                if is_prime(third) and get_digits(third) == digits:
                    concat = int(f'{first}{second}{third}')
                    if concat != 148748178147:
                        yield concat

try:
    print(next(make_sequences()))
except (OSError, KeyboardInterrupt):
    pass
