#!/usr/bin/env elixir

# A palindromic number reads the same both ways. The largest palindrome
# made from the product of two 2-digit numbers is 9009 = 91 x 99.
#
# Find the largest palindrome made from the product of two 3-digit numbers.

defmodule Test do

  def is_palindrome?(number) do
    number = to_string(number)
    String.reverse(number) == number
  end

  def find_product(first, maximum) do
    product = fn x -> first * x end
    case maximum..1 |> Stream.map(product) |> Stream.filter(&is_palindrome?/1) |> Enum.fetch(0) do
      {:ok, result} -> result
      :error -> nil
    end
  end

end

999..1
|> Stream.map(fn x -> Test.find_product(x, 999) end)
|> Stream.filter(fn x -> !is_nil(x) end)
|> Enum.max
|> IO.puts
