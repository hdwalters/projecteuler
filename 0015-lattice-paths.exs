#!/usr/bin/env elixir
Code.require_file "memoize.ex", "lib"

# Starting in the top left corner of a 2x2 grid, and only being able to
# move to the right and down, there are exactly 6 routes to the bottom
# right corner.
#
#   x--->--->   x---> . x   x---> . x
#   . . . . |   . . | . .   . . | . .
#   x . x . v   x . v--->   x . v . x
#   . . . . |   . . . . |   . . | . .
#   x . x . v   x . x . v   x . v--->
#
#   x . x . x   x . x . x   x . x . x
#   | . . . .   | . . . .   | . . . .
#   v--->--->   v---> . x   v . x . x
#   . . . . |   . . | . .   | . . . .
#   x . x . v   x . v--->   v--->--->
#
# How many such routes are there through a 20x20 grid?
#
#   x--->--->--->   x--->---> . x   x--->---> . x   x--->---> . x
#   . . . . . . |   . . . . | . .   . . . . | . .   . . . . | . .
#   x . x . x . v   x . x . v--->   x . x . v . x   x . x . v . x
#   . . . . . . |   . . . . . . |   . . . . | . .   . . . . | . .
#   x . x . x . v   x . x . x . v   x . x . v--->   x . x . v . x
#   . . . . . . |   . . . . . . |   . . . . . . |   . . . . | . .
#   x . x . x . v   x . x . x . v   x . x . x . v   x . x . v--->
#
#   x---> . x . x   x---> . x . x   x---> . x . x   x---> . x . x
#   . . | . . . .   . . | . . . .   . . | . . . .   . . | . . . .
#   x . v--->--->   x . v---> . x   x . v---> . x   x . v . x . x
#   . . . . . . |   . . . . | . .   . . . . | . .   . . | . . . .
#   x . x . x . v   x . x . v--->   x . x . v . x   x . v--->--->
#   . . . . . . |   . . . . . . |   . . . . | . .   . . . . . . |
#   x . x . x . v   x . x . x . v   x . x . v--->   x . x . x   v
#
#   x---> . x . x   x---> . x . x   x . x . x . x   x . x . x . x
#   . . | . . . .   . . | . . . .   | . . . . . .   | . . . . . .
#   x . v . x . x   x . v . x . x   v--->--->--->   v--->---> . x
#   . . | . . . .   . . | . . . .   . . . . . . |   . . . . | . .
#   x . v---> . x   x . v . x . x   x . x . x . v   x . x . v--->
#   . . . . | . .   . . | . . . .   . . . . . . |   . . . . . . |
#   x . x . v--->   x . v--->--->   x . x . x . v   x . x . x . v
#
#   x . x . x . x   x . x . x . x   x . x . x . x   x . x . x . x
#   | . . . . . .   | . . . . . .   | . . . . . .   | . . . . . .
#   v--->---> . x   v---> . x . x   v---> . x . x   v---> . x . x
#   . . . . | . .   . . | . . . .   . . | . . . .   . . | . . . .
#   x . x . v . x   x . v--->--->   x . v---> . x   x . v . x . x
#   . . . . | . .   . . . . . . |   . . . . | . .   . . | . . . .
#   x . x . v--->   x . x . x . v   x . x . v--->   x . v--->--->
#
#   x . x . x . x   x . x . x . x   x . x . x . x   x . x . x . x
#   | . . . . . .   | . . . . . .   | . . . . . .   | . . . . . .
#   v . x . x . x   v . x . x . x   v . x . x . x   v . x . x . x
#   | . . . . . .   | . . . . . .   | . . . . . .   | . . . . . .
#   v--->--->--->   v--->---> . x   v---> . x . x   v . x . x . x
#   . . . . . . |   . . . . | . .   . . | . . . .   | . . . . . .
#   x . x . x . v   x . x . >--->   x . v--->--->   v--->--->--->
#
# 0 => 1
# 1 => 2
# 2 => 6
# 3 => 20
# 4 => 70
# 5 => 252
# 6 => 924
# 7 => 3432
# 8 => 12870
# 9 => 48620
# 10 => 184756
# 11 => 705432
# 12 => 2704156
# 13 => 10400600
# 14 => 40116600
# 15 => 155117520
# 16 => 601080390
# 17 => 2333606220
# 18 => 9075135300
# 19 => 35345263800
# 20 => 137846528820

defmodule Test do

  def get_routes(pid, rows, cols) do
    Memoize.memoize pid, {rows, cols}, fn ->
      case {rows, cols} do
        {0, _} -> 1
        {_, 0} -> 1
        {rows, cols} -> get_routes(pid, rows - 1, cols) + get_routes(pid, rows, cols - 1)
      end
    end
  end

end

pid = Memoize.start_link!
IO.puts Test.get_routes(pid, 20, 20)
