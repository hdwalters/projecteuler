#!/usr/bin/env python3

# In the United Kingdom the currency is made up of pound (£) and pence
# (p). There are eight coins in general circulation:
#
#   1p, 2p, 5p, 10p, 20p, 50p, £1 (100p), and £2 (200p).
#
# It is possible to make £2 in the following way:
#
#   1x£1 + 1x50p + 2x20p + 1x5p + 1x2p + 3x1p
#
# How many different ways can £2 be made using any number of coins?

COINS = (200, 100, 50, 20, 10, 5, 2, 1)

def count_combinations(parent, start):
    count = 0
    total = sum(x * y for x, y in zip(COINS, parent))
    for index in range(start, len(COINS)):
        if total + COINS[index] == 200:
            count += 1
        elif total + COINS[index] < 200:
            child = create_combination(parent, index)
            count += count_combinations(child, index)
    return count

def create_combination(parent, index):
    modify = lambda i, x: x + 1 if i == index else x
    return tuple(modify(i, x) for i, x in enumerate(parent))

try:
    print(count_combinations((0, 0, 0, 0, 0, 0, 0, 0), 0))
except (OSError, KeyboardInterrupt):
    pass
