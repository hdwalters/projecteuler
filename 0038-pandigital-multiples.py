#!/usr/bin/env python3

# Take the number 192 and multiply it by each of 1, 2, and 3:
#
#   192 * 1 = 192
#   192 * 2 = 384
#   192 * 3 = 576
#
# By concatenating each product we get the 1 to 9 pandigital 192384576.
# We will call 192384576 the concatenated product of 192 and (1, 2, 3).
#
# The same can be achieved by starting with 9 and multiplying by 1, 2, 3,
# 4, and 5, giving the pandigital 918273645, which is the concatenated
# product of 9 and (1, 2, 3, 4, 5).
#
# What is the largest 1 to 9 pandigital that can be formed as the
# concatenated product of an integer with (1, 2, ..., n) where n > 1?

MAXIMUM = 1_000_000_000

def is_pandigital(number):
    digits = sorted(str(number))
    return digits == ['1', '2', '3', '4', '5', '6', '7', '8', '9']

def get_product(number, count):
    parts = [str(number * n) for n in range(1, count + 1)]
    return int(''.join(parts))

def find_upper(lower, upper, count):
    if upper - lower <= 2:
        return upper
    else:
        middle = (lower + upper) // 2
        product = get_product(middle, count)
        if product >= MAXIMUM:
            return find_upper(lower, middle, count)
        else:
            return find_upper(middle, upper, count)

def find_candidate(count):
    upper = find_upper(0, MAXIMUM, count)
    for number in range(upper, 0, -1):
        product = get_product(number, count)
        if product < MAXIMUM // 10:
            break
        if is_pandigital(product):
            return product
    return 0

try:
    print(max(find_candidate(count) for count in range(2, 10)))
except (OSError, KeyboardInterrupt):
    pass
