#!/usr/bin/env python3
from lib.primes import *

# It was proposed by Christian Goldbach that every odd composite number
# can be written as the sum of a prime and twice a square:
#
#    9 =  7 + (2 * 1^2)
#   15 =  7 + (2 * 2^2)
#   21 =  3 + (2 * 3^2)
#   25 =  7 + (2 * 3^2)
#   27 = 19 + (2 * 2^2)
#   33 = 31 + (2 * 1^2)
#
# It turns out that the conjecture was false.
#
# What is the smallest odd composite that cannot be written as the sum
# of a prime and twice a square?

def make_candidates():
    for number in make_composites():
        if number % 2 == 1:
            yield number

def not_goldbach(number):
    for root in itertools.count(1):
        prime = number - (root * root * 2)
        if prime <= 0:
            return True
        if is_prime(prime):
            return False

def make_values():
    for number in make_candidates():
        if not_goldbach(number):
            yield number

try:
    print(next(make_values()))
except (OSError, KeyboardInterrupt):
    pass
