#!/usr/bin/env python3
from functools import reduce
from itertools import repeat

# The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.
#
# Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.

def modulo_mul(first, second):
    return (first * second) % 10_000_000_000

def get_power(number):
    return reduce(modulo_mul, repeat(number, number))

try:
    total = sum(get_power(n) for n in range(1, 1000))
    print(total % 10_000_000_000)
except (OSError, KeyboardInterrupt):
    pass
