#!/usr/bin/env elixir

# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
#
# Find the sum of all the primes below two million.

defmodule Test do

  def make_primes(size) do
    primes = Enum.to_list(Stream.concat([0..0, 0..0, 2..(size - 1)]))
    sqrt = round(:math.sqrt(size))
    scan_list(primes, 0, sqrt)
  end

  defp scan_list(primes, index, sqrt) do
    result = case primes do
      [head | tail] when head == 0 ->
        [head | scan_list(tail, index + 1, sqrt)]
      [head | tail] when index <= sqrt ->
        tail = sieve_list(tail, index + 1, index)
        [head | scan_list(tail, index + 1, sqrt)]
      rest ->
        rest
    end
    result
  end

  defp sieve_list(primes, index, factor) do
    result = case primes do
      [_head | tail] when rem(index, factor) == 0 ->
        [0 | sieve_list(tail, index + 1, factor)]
      [head | tail] ->
        [head | sieve_list(tail, index + 1, factor)]
      rest ->
        rest
    end
    result
  end

end

Test.make_primes(2000000) |> Enum.sum |> IO.puts
