#!/usr/bin/env elixir

# n! means n * (n − 1) * ... * 3 * 2 * 1
#
# For example, 10! = 10 * 9 * ... * 3 * 2 * 1 = 3628800, and the sum of
# the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
#
# Find the sum of the digits in the number 100!

defmodule Test do

  def fact(number) do
    fact(number, 1)
  end

  defp fact(number, acc) do
    case number do
      1 -> acc
      n -> fact(n - 1, acc * n)
    end
  end

end

Test.fact(100)
|> Integer.digits
|> Enum.sum
|> IO.puts
