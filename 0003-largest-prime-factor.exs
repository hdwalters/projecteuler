#!/usr/bin/env elixir

# The prime factors of 13195 are 5, 7, 13 and 29.
#
# What is the largest prime factor of the number 600851475143?

defmodule Test do

  def find_factor(number) do
    half = div(number, 2)
    is_divisible? = fn x -> rem(number, x) == 0 end
    case 2..half |> Stream.filter(is_divisible?) |> Enum.fetch(0) do
      {:ok, factor} -> find_factor(div(number, factor))
      :error -> number
    end
  end

end

IO.puts Test.find_factor(600851475143)
