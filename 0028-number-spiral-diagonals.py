#!/usr/bin/env python3
from lib.spirals import make_diagonals

# Starting with the number 1 and moving to the right in an anticlockwise
# direction a 5 by 5 spiral is formed as follows:
#
#   [37]  36   35   34   33   32  [31]
#    38  [17]  16   15   14  [13]  30
#    39   18  [05]  04  [03]  12   29
#    40   19   06  [01]  02   11   28
#    41   20  [07]  08  [09]  10   27
#    42  [21]  22   23   24  [25]  26
#   [43]  44   45   46   47   48  [49]
#
# It can be verified that the sum of the numbers on the diagonals is 101.
#
# What is the sum of the numbers on the diagonals in a 1001 by 1001
# spiral formed in the same way?

def make_indices(size):
    for row, col, index in make_diagonals():
        if index > size * size:
            break
        yield index

try:
    print(sum(make_indices(1001)))
except (OSError, KeyboardInterrupt):
    pass
