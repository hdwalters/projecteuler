#!/usr/bin/env python3
from functools import reduce
from operator import mul

# An irrational decimal fraction is created by concatenating the positive
# integers:
#
#   0.123456789101112131415161718192021...
#                ^
#     000000000111111
#     123456789012345
#
# It can be seen that the 12th digit of the fractional part is 1.
#
# If d[n] represents the nth digit of the fractional part, find the value
# of the following expression:
#
#   d[1] * d[10] * d[100] * d[1000] * d[10000] * d[100000] * d[1000000]

def make_digits():
    for number in range(1000000):
        yield from str(number)

try:
    indices = {1, 10, 100, 1000, 10000, 100000, 1000000}
    digits = (int(d) for i, d in enumerate(make_digits()) if i in indices)
    print(reduce(mul, digits))
except (OSError, KeyboardInterrupt):
    pass
