#!/usr/bin/env elixir

# If the numbers 1 to 5 are written out in words: one, two, three, four,
# five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
#
# If all the numbers from 1 to 1000 (one thousand) inclusive were written
# out in words, how many letters would be used?
#
# N.B. Do not count spaces or hyphens. For example, 342 (three hundred
# and forty two) contains 23 letters and 115 (one hundred and fifteen)
# contains 20 letters.  The use of "and" when writing out numbers is in
# compliance with British usage.

defmodule Test do

  defp list_units do
    [nil, "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
  end

  defp list_teens do
    ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
  end

  defp list_tens do
    ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
  end

  defp make_tens do
    Stream.flat_map(list_tens(), fn ten -> Stream.map(list_units(), fn unit -> join_words(ten, unit) end) end)
  end

  defp make_hundred do
    Stream.concat([list_units(), list_teens(), make_tens()])
  end

  def make_thousand do
    stream = Stream.flat_map(list_units(), fn unit -> Stream.map(make_hundred(), fn hun -> join_hundred(unit, hun) end) end)
    Stream.concat(stream, ["one thousand"])
  end

  defp join_words(nil, nil), do: ""
  defp join_words(nil, second), do: second
  defp join_words(first, nil), do: first
  defp join_words(first, second), do: [first, " ", second]

  defp join_hundred(nil, nil), do: nil
  defp join_hundred(nil, second), do: second
  defp join_hundred(first, nil), do: [first, " hundred"]
  defp join_hundred(first, second), do: [first, " hundred and ", second]

end

Test.make_thousand
|> Stream.filter(fn x -> !is_nil(x) end)
|> Stream.map(fn x -> IO.iodata_to_binary(x) end)
|> Stream.flat_map(fn x -> String.codepoints(x) end)
|> Stream.filter(fn x -> x >= "a" && x <= "z" end)
|> Enum.count
|> IO.puts
