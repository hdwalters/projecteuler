#!/usr/bin/env elixir

# Using names.txt, a 46K text file containing over five-thousand first
# names, begin by sorting it into alphabetical order. Then working out the
# alphabetical value for each name, multiply this value by its alphabetical
# position in the list to obtain a name score.
#
# For example, when the list is sorted into alphabetical order, COLIN,
# which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list.
# So, COLIN would obtain a score of 938 * 53 = 49714.
#
# What is the total of all the name scores in the file?

dirname = Path.dirname(__ENV__.file)
basename = Path.rootname(Path.basename(__ENV__.file))
path = Path.join([dirname, "data", "#{basename}.txt"])

File.stream!(path)
|> Enum.sort
|> Stream.map(fn x -> String.trim_trailing(x) end)
|> Stream.map(fn x -> String.to_charlist(x) end)
|> Stream.map(fn x -> Enum.map(x, fn y -> y - ?A + 1 end) end)
|> Stream.map(fn x -> Enum.sum(x) end)
|> Stream.with_index(1)
|> Stream.map(fn {x, n} -> x * n end)
|> Enum.sum
|> IO.puts
