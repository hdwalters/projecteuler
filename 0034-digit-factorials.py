#!/usr/bin/env python3
from functools import *
from itertools import *
from operator import *

# 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
#
# Find the sum of all numbers which are equal to the sum of the factorial
# of their digits.
#
# As 1! = 1 and 2! = 2 are not sums they are not included.
#
# N.B. 9,999,999 and larger cannot be a curious number, because the sum
# of its factorial digits (all 9s) is less than the original number, and
# every time we append a new digit, the original number will grow even
# more.

FACTORIALS = list(accumulate(range(1, 10), mul, initial=1))

def test_factorial(number):
    total = sum(FACTORIALS[int(n)] for n in str(number))
    return total == number

try:
    print(sum(n for n in range(3, 10_000_000) if test_factorial(n)))
except (OSError, KeyboardInterrupt):
    pass
