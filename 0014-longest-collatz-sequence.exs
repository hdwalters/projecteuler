#!/usr/bin/env elixir
Code.require_file "memoize.ex", "lib"

# The following iterative sequence is defined for the set of positive
# integers:
#
# n => n / 2 (n is even)
# n => 3n + 1 (n is odd)
#
# Using the rule above and starting with 13, we generate the following
# sequence:
#
# 13 => 40 => 20 => 10 => 5 => 16 => 8 => 4 => 2 => 1
#
# It can be seen that this sequence (starting at 13 and finishing at 1)
# contains 10 terms. Although it has not been proved yet (Collatz Problem),
# it is thought that all starting numbers finish at 1.
#
# Which starting number, under one million, produces the longest chain?
#
# N.B. Once the chain starts the terms are allowed to go above one million.

defmodule Test do

  def get_maximum(pid, limit) do
    Stream.iterate(1, fn x -> x + 1 end)
    |> Stream.take_while(fn x -> x < limit end)
    |> Stream.map(fn x -> {get_length(pid, x), x} end)
    |> Enum.max
    |> elem(1)
  end

  defp get_length(pid, number) do
    Memoize.memoize pid, number, fn ->
      case number do
        1 -> 1
        n when rem(n, 2) == 0 -> get_length(pid, div(n, 2)) + 1
        n when rem(n, 2) == 1 -> get_length(pid, (3 * n) + 1) + 1
      end
    end
  end

end

pid = Memoize.start_link!
IO.puts Test.get_maximum(pid, 1000000)
