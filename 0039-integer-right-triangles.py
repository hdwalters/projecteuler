#!/usr/bin/env python3

# If p is the perimeter of a right angle triangle with integral length
# sides {a, b, c}, there are exactly three solutions for p = 120:
#
#   {20, 48, 52}, {24, 45, 51}, {30, 40, 50}
#
# For which value of p <= 1000, is the number of solutions maximised?
#
# N.B. Given a specific perimeter p and side a, calculate the remaining
# sides b and c by solving these equations:
#
#   a^2 + b^2 = c^2
#   a + b + c = p
#
#   a^2 + b^2 = ((p - a) - b)^2
#   a^2 + b^2 = p^2 - 2pa + a^2 - 2(p - a)b + b^2
#           0 = p^2 - 2pa       - 2(p - a)b
#
#               p^2 - 2pa
#           b = ---------
#               2(p - a)

def calculate_b(p, a):
    return int(round(((p * p) - (2 * p * a)) / (2 * (p - a))))

def calculate_c(p, a, b):
    return p - a - b

def is_candidate(p, a):
    b = calculate_b(p, a)
    c = calculate_c(p, a, b)
    return b, c if (a * a) + (b * b) == (c * c) else None

def make_candidates(p):
    for a in range(1, p):
        b, c = is_candidate(p, a)
        if b < a:
            break
        if b and c:
            yield a, b, c

def count_candidates(p):
    return sum(1 for _ in make_candidates(p))

try:
    print(max(range(1, 1001), key=count_candidates))
except (OSError, KeyboardInterrupt):
    pass
