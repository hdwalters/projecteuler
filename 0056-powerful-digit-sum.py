#!/usr/bin/env python3

# A googol 10^100 is a massive number, one followed by one hundred zeroes;
# 100^100 is almost unimaginably large, one followed by two hundred zeroes.
# Despite their size, the sum of the digits in each number is only 1.
#
# Considering natural numbers of the form a^b, where a < 100 and b < 100,
# what is the maximum digital sum?

def make_digital_sums():
    for base in range(1, 100):
        for exp in range(1, 100):
            power = pow(base, exp)
            yield sum(int(x) for x in str(power))

try:
    print(max(make_digital_sums()))
except (OSError, KeyboardInterrupt):
    pass
