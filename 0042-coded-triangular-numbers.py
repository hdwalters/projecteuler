#!/usr/bin/env python3
from lib.data import read_lines
from lib.figurates import is_triangular

# The nth term of the sequence of triangular numbers is given by T[n] =
# n(n + 1) / 2, so the first ten triangular numbers are:
#
#   1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
#
# By converting each letter in a word to a number corresponding to its
# alphabetical position and adding these values we form a word value.
# For example, the word value for SKY is 19 + 11 + 25 = 55 = T[10].  If
# the word value is a triangular number then we shall call the word a
# triangular word.
#
# Using words.txt, a 16K text file containing nearly two-thousand common
# English words, how many are triangular words?

def make_numbers():
    for line in read_lines():
        number = sum(ord(c) - 64 for c in line if 'A' <= c <= 'Z')
        if is_triangular(number):
            yield line

try:
    print(sum(1 for _ in make_numbers()))
except (OSError, KeyboardInterrupt):
    pass
